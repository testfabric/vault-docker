# Vault Docker

A lightweight hashicorp vault image, built entirely from source. Intended to conservatively track upstream vault.

## Running

```
docker run --cap-add=IPC_LOCK -p8200:8200 registry.gitlab.com/testfabric/vault-docker
```
