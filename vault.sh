#!/bin/sh
set -e

# Setup file permissions
chown -R vault:vault /secrets

# Drop privileges and run vault
sudo -u vault /usr/local/bin/vault server -config=/etc/vault.d/vault.hcl
