FROM debian:stretch-slim

EXPOSE 8200

ENV DEBIAN_FRONTEND noninteractive

ADD "https://releases.hashicorp.com/vault/1.1.3/vault_1.1.3_linux_amd64.zip" vault_1.1.3_linux_amd64.zip

COPY packages.sha256 packages.sha256
COPY vault.sh /vault.sh

RUN sha256sum -c packages.sha256 \
  && apt-get update \
  && apt-get install -y dumb-init libcap2-bin unzip sudo \
  && unzip vault_1.1.3_linux_amd64.zip -d /usr/local/bin \
  && chmod +x /usr/local/bin/vault \
  && setcap cap_ipc_lock=+ep /usr/local/bin/vault \
  && useradd --system --home /etc/vault.d --shell /bin/false vault \
  && chmod +x /vault.sh \
  && mkdir /etc/vault.d /secrets \
  && rm -f packages.sha256 vault*.zip \
  && apt-get clean

ENTRYPOINT [ "/usr/bin/dumb-init", "--" ]
CMD [ "/vault.sh" ]
